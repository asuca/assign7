package assign7;

import java.time.LocalDate;

public class User {
	int id;
	String name;
	LocalDate birthdate;
	
	public User(int id, String name, LocalDate birthdate) {
		this.id        = id;
		this.name      = name;
		this.birthdate = birthdate;
	}
	
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public LocalDate getBirthdate() {
		return birthdate;
	}
	
	public String toString() {
		return String.format("id = %d, name = %s, "
				+ "birthday = %s",this.id,this.name,this.birthdate);
	}
}