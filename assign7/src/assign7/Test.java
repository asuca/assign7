package assign7;

import java.time.LocalDate;
import java.util.*;

public class Test {
	
	public static void main(String[] args) {
		// List setting
		List<User> ulist = new ArrayList<User>();
		// 5 User
		ulist.add(new User(1,"jim",LocalDate.parse("2020-01-08")));
		ulist.add(new User(5,"john",LocalDate.parse("2020-02-08")));
		ulist.add(new User(3,"billy",LocalDate.parse("2020-03-08")));
		ulist.add(new User(2,"lyle",LocalDate.parse("2020-04-08")));
		ulist.add(new User(4,"tom",LocalDate.parse("2020-05-08")));
        //
		System.out.println("-----sort by birthday -----");
		sortListByBirthday(ulist);
		//
		System.out.println("-----sort by id -----");
		sortId(ulist);
		//
		System.out.println("-----sort by name -----");
		sortName(ulist);
	}	
	//Sort by Birthday
	public static void sortListByBirthday(List<User> ulist){
		Comparator<User> byBirthday = (c1,c2) -> {
			if(c1.getBirthdate().isAfter(c2.getBirthdate())) return -1;
			else return 1;
		};	
		Collections.sort(ulist,byBirthday);
		for (int i = 0; i < ulist.size(); i++) {
			System.out.println(ulist.get(i));
		}
	}
	//Sort by id
	public static void sortId(List<User> ulist){
		ulist.sort((User u1,User u2)->u1.getId()-u2.getId());
		ulist.forEach((u)->System.out.println(u));
	}
	//Sort by name
	public static void sortName(List<User> ulist){
		ulist.sort((User u1,User u2)->u1.getName().compareTo(u2.getName()));
		ulist.forEach((u)->System.out.println(u));
	}
}

